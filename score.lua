function drawScore()
	local score = math.abs(math.floor(currentScore))

	if score > 0 and not Intro.active then
		if not alarmlevel then return end
		if alarmlevel == 0 then
			love.graphics.setColor(96,255,96)
		elseif alarmlevel == 1 then
			love.graphics.setColor(255,220,64)
		elseif alarmlevel == 2 then
			love.graphics.setColor(255,64,64)
		end

		local cyphers = math.floor(math.log(score)/math.log(10))+1
		love.graphics.print(math.floor(currentScore), screenSize.x/2 - 42*cyphers/2, 16)
	end
end

function initHighscore()
	Highscore = {}
	slots = 5
	loadHighscore()
end

function storeScore()

	local score = getScore()

	table.insert(Highscore, score)
	table.sort(Highscore, function(a,b) return a>b end)

	while table.getn(Highscore)>slots do
		table.remove(Highscore,table.getn(Highscore))
	end

	saveHighscore()
end

function getScore()
	return math.abs(math.floor(currentScore))
end

function drawHighscore(x,y, opacity)
	opacity = opacity or 1
	local score = getScore()
	for i,v in ipairs(Highscore) do
		if score == v then
			love.graphics.setColor(96,240,255,255*opacity)
		else
			love.graphics.setColor(255,255,255,255*opacity)
		end

		love.graphics.print(i..".",x,y + (i-1)*50)
		local cyphers = math.floor(math.log(v)/math.log(10))+1
		love.graphics.print(v, x+900-cyphers*42, y+(i-1)*50)
	end

	love.graphics.setColor(255,255,255)
end

function saveHighscore()
	local st = "Highscore={"
	for i,v in ipairs(Highscore) do
		st = st..v
		if i<table.getn(Highscore) then
			st = st..","
		end
	end
	st = st.."}"
	love.filesystem.write("scores.dat",st,string.len(st))
end

function loadHighscore()
	local dta = love.filesystem.load("scores.dat")
	if dta then dta()
		if table.getn(Highscore)>0 then

		table.sort(Highscore, function(a,b) return a>b end)

		while table.getn(Highscore)>slots do
			table.remove(Highscore,table.getn(Highscore))
		end
	end
	end
end