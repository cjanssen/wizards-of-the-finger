function initIntro()
	Intro = {
		lefthandImg = love.graphics.newImage("img/handleft.png"),
		righthandImg = love.graphics.newImage("img/handright.png"),
		logoImg = love.graphics.newImage("img/logo.png"),
		backbuttonImg = love.graphics.newImage("img/buttonback.png"),
		howtobuttonImg = love.graphics.newImage("img/buttonhowtoplay.png"),
		teambuttonImg = love.graphics.newImage("img/buttonteam.png"),
		howtoScreen = love.graphics.newImage("img/howtoscreen.png"),
		taptext = love.graphics.newImage("img/taptostart.png"),
		yourscoretext = love.graphics.newImage("img/yourscore.png"),
		highscoretext = love.graphics.newImage("img/highscore.png"),
		creditsScreen = love.graphics.newImage("img/credits.png"),
		butcoords = {(32-19)*6,(160-19)*6,(32+19)*6,(160+19)*6},
		cbutcoords = {(285-19)*6,(160-19)*6,(285+19)*6,(160+19)*6},
		blinktimer = 0,
		blinkdelay = 2
	}
end

function startIntro()
	Intro.active = true
	Intro.fading = false
	Intro.opacity = 1
	Intro.textOpacity = 0
	vibration = 0
	Intro.currentScreen = 1
	Intro.lefthandOrig = -82*6
	Intro.lefthandDest = 0
	Intro.lefthandPos = Intro.lefthandOrig
	Intro.righthandOrig = screenSize.x
	Intro.righthandDest = screenSize.x-Intro.righthandImg:getWidth()*spriteScale.x
	Intro.righthandPos = Intro.righthandOrig
	Intro.logoOrig = -74*6
	Intro.logoDest = 6*6
	Intro.logoPos = Intro.logoOrig
	Intro.timer = 0

	Sounds.maintheme:rewind()
	Sounds.maintheme:play()
	Sounds.maintheme:setVolume(1)

end

function updateIntro(dt)
	Intro.timer = Intro.timer + dt

	local handsStartTime = 0.8
	local handsDuration = 0.4
	if Intro.timer >= handsStartTime and Intro.timer < handsStartTime+handsDuration then
		local factor = (Intro.timer - handsStartTime) / handsDuration
		Intro.lefthandPos = Intro.lefthandOrig + factor * (Intro.lefthandDest - Intro.lefthandOrig)
		Intro.righthandPos = Intro.righthandOrig + factor * (Intro.righthandDest - Intro.righthandOrig)
	elseif Intro.timer >= handsStartTime+handsDuration then
		Intro.lefthandPos = Intro.lefthandDest
		Intro.righthandPos = Intro.righthandDest
	else
		Intro.lefthandPos = Intro.lefthandOrig
		Intro.righthandPos = Intro.righthandOrig
	end


	local logoStartTime = 0
	local logoDuration = 0.4
	if Intro.timer < logoStartTime+logoDuration then
		local factor = (Intro.timer - logoStartTime) / logoDuration
		Intro.logoPos = Intro.logoOrig + factor* (Intro.logoDest - Intro.logoOrig)
	elseif Intro.timer >= logoStartTime+logoStartTime then
		Intro.logoPos = Intro.logoDest
	else
		Intro.logoPos = Intro.logoOrig
	end

	if Intro.timer >= 1.6 then
		Intro.textOpacity = increaseExponential(dt, Intro.textOpacity, 0.91)
	end

	if Intro.fading then
		Intro.opacity = decreaseExponential(dt,Intro.opacity,0.93)
		if Intro.opacity < 0.05 then
			Intro.fading = false
		end
	end

	Intro.blinktimer = Intro.blinktimer + dt
	if Intro.blinktimer >= Intro.blinkdelay then
		Intro.blinktimer = Intro.blinktimer - Intro.blinkdelay
	end
end

function introTapped(_x,_y)
	if Intro.textOpacity < 1 then
		return
	end

	if _x >= Intro.butcoords[1] and _x<=Intro.butcoords[3] and _y >= Intro.butcoords[2] and _y <= Intro.butcoords[4] then
		-- buttonpressed
		if Intro.currentScreen == 1 then
			Intro.currentScreen = 2
		elseif Intro.currentScreen == 2 then
			Intro.currentScreen = 1
		end
		return
	elseif _x >= Intro.cbutcoords[1] and _x<=Intro.cbutcoords[3] and _y >= Intro.cbutcoords[2] and _y <= Intro.cbutcoords[4] then
		-- credits button
		if Intro.currentScreen == 1 then
			Intro.currentScreen = 3
		elseif Intro.currentScreen == 3 then
			Intro.currentScreen = 1
		end
		return
	end

	-- else
	if Intro.currentScreen == 1 then
		Intro.active = false
		Intro.fading = true
		startGame()
	end
end

function introUntapped(_x,_y)
end

function drawIntro()
	love.graphics.setColor(255,255,255)
	if Intro.currentScreen == 1 then
		if Intro.fading then
			love.graphics.setColor(255,255,255,255*Intro.opacity)
		else
			love.graphics.draw(bgImg,0,0,0,spriteScale.x,spriteScale.y)
		end

		-- hands and logo
		love.graphics.draw(Intro.lefthandImg,Intro.lefthandPos,0,0,spriteScale.x,spriteScale.y)
		love.graphics.draw(Intro.righthandImg,Intro.righthandPos,0,0,spriteScale.x,spriteScale.y)
		love.graphics.draw(Intro.logoImg,screenSize.x/2,Intro.logoPos,0,spriteScale.x,spriteScale.y,Intro.logoImg:getWidth()/2)

		love.graphics.setColor(255,255,255,255*Intro.opacity*Intro.textOpacity)
		-- howto button
		love.graphics.draw(Intro.howtobuttonImg, 32*6, 160*6, 0, spriteScale.x, spriteScale.y, Intro.howtobuttonImg:getWidth()/2, Intro.howtobuttonImg:getHeight()/2)
		love.graphics.draw(Intro.teambuttonImg, 285*6, 160*6, 0, spriteScale.x, spriteScale.y, Intro.teambuttonImg:getWidth()/2, Intro.teambuttonImg:getHeight()/2)

		-- your score
		local sc = getScore()
		if sc > 0 then
			love.graphics.draw(Intro.yourscoretext, 500, 900, 0, spriteScale.x, spriteScale.y)
		
			local cyphers = 1
			if sc > 1 then cyphers = math.floor(math.log(sc)/math.log(10))+1 end
			love.graphics.print(sc, 1400-cyphers*42, 900)
		end

		-- highscore table
		if table.getn(Highscore) > 0 then
			love.graphics.draw(Intro.highscoretext, screenSize.x/2, 500, 0, spriteScale.x, spriteScale.y, Intro.highscoretext:getWidth()/2, 0)
			drawHighscore(500,570, Intro.opacity*Intro.textOpacity)
		end

		-- tapto
		if Intro.blinktimer < Intro.blinkdelay / 2 then
			love.graphics.setColor(255,255,255,255*Intro.opacity*Intro.textOpacity)
			love.graphics.draw(Intro.taptext, screenSize.x/2, 1000, 0, spriteScale.x, spriteScale.y, Intro.taptext:getWidth()/2, 0)
		end

	elseif Intro.currentScreen == 2 then
		love.graphics.draw(Intro.howtoScreen,0,0,0,spriteScale.x,spriteScale.y)
		love.graphics.draw(Intro.backbuttonImg, 32*6, 160*6, 0, spriteScale.x, spriteScale.y, Intro.backbuttonImg:getWidth()/2, Intro.backbuttonImg:getHeight()/2)
	elseif Intro.currentScreen == 3 then
		love.graphics.draw(bgImg,0,0,0,spriteScale.x,spriteScale.y)
		love.graphics.draw(Intro.creditsScreen,screenSize.x/2,screenSize.y/2,0,spriteScale.x,spriteScale.y,Intro.creditsScreen:getWidth()/2,Intro.creditsScreen:getHeight()/2)
		love.graphics.draw(Intro.backbuttonImg, 285*6, 160*6, 0, spriteScale.x, spriteScale.y, Intro.backbuttonImg:getWidth()/2, Intro.backbuttonImg:getHeight()/2)
	end
end
