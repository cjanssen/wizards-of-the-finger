function initPlayers()
	Players = {}
	table.insert(Players,newPlayer(1440,600,"img/spriteRed.png"))
	table.insert(Players,newPlayer(480,600,"img/spriteBlue.png"))
	-- table.insert(Players,newPlayer(100,400,{255,255,0}))
	-- table.insert(Players,newPlayer(600,100,{0,255,0}))

	currentScore = 0
	Players.deathsprites = getSprites("img/playerdeath.png")
	Players.deathsprites.loops = false
	rebootTimer = 0

	ExtraImgs = {
		infop1 = love.graphics.newImage("img/p1start.png"),
		infop2 = love.graphics.newImage("img/p2start.png"),
		arrow = love.graphics.newImage("img/arrow.png"),
		arrowphase = 0,
		arrowfreq = 4
	}
end

function playerColor(n)
	if n == 1 then
		return {255,0,0}
	elseif n == 2 then
		return {0,0,255}
	end
end

function startPlayers()
	for i,v in ipairs(Players) do
		v.pos = {x = v.startPos.x, y = v.startPos.y}
		v.newPos = {x = v.startPos.x, y = v.startPos.y}
		v.deathtimer = Players.deathsprites.framecount * Players.deathsprites.framedelay
		v.dead = false
		v.opacity = 0
		v.touched = false
	end
	currentScore = 0
	gameon = false
	started = false

	startPlayerSpawnSprites()
end

function startPlayerSpawnSprites()
	Players.spawnsprites = {
		referSprites(Enemies.spawnsprites),
		referSprites(Enemies.spawnsprites)
	}
	for i,v in ipairs(Players.spawnsprites) do
		v.framendx = 9
		v.loops = false
		v.timer = (v.framecount - 9)*v.framedelay
		v.pos = { x = Players[i].pos.x, y = Players[i].pos.y + 50 }
	end
end

function newPlayer(x_,y_,fname_)
	local p = {
		sprites = getSprites(fname_),
		pos = { x = x_, y = y_},
		rad = 70,
		det = 140*140,
		startPos = { x = x_, y = y_},
		newPos = { x = x_, y = y_},
		opacity = 0
	}
	return p
end

function killPlayer(player)
	playSound(Sounds.deathplayer)
	gameon = false
	player.dead = true
	Players.deathsprites.framendx = 1
	rebootTimer = player.deathtimer * 2
	storeScore()
end

function updatePlayers(dt)
	if gameon then
		local distcapped = min(cappedDist(Players[1].pos, Players[2].pos, Players[1].rad,Players[2].rad), screenSize.x/2)
		currentScore = currentScore + 20 * dt * distcapped / (screenSize.x/2)

		if distcapped >= screenSize.x/4 then
			vibration = 0
		else
			local normdist = (screenSize.x/4 - distcapped) / (screenSize.x/4)
			vibration = normdist * normdist * 40
			if Sounds.tooclose:isStopped() then
				Sounds.tooclose:play()
			end
			Sounds.tooclose:setVolume(normdist)
		end

		-- alarm level (score color)
		if distcapped >= screenSize.x*3/8 then
			alarmlevel = 0
		elseif distcapped >= screenSize.x*1/8 then
			alarmlevel = 1
		else
			alarmlevel = 2
		end

		-- spawnanims
		for i,v in ipairs(Players.spawnsprites) do
			if v then
				v.timer = v.timer - dt
				if v.timer <= 0 then
					Players.spawnsprites[i] = false
				else
					updateAnim(dt,v)
				end
			end
		end
	else
		if started then
			rebootTimer = rebootTimer - dt
			if rebootTimer <= 0 then
				Sounds.tooclose:setVolume(0)
				startIntro()
			end
		else
			-- arrow movement
			ExtraImgs.arrowphase = (ExtraImgs.arrowphase + ExtraImgs.arrowfreq * dt) % (math.pi*2)

			local desiredVol = 1

			-- starting
			local visiblePlayers = 0
			local semiVisiblePlayers = 0
			for i,v in ipairs(Players) do
				if v.touched then
					v.opacity = increaseExponential(dt, v.opacity, 0.97)
				else
					if ontablet then
						v.opacity = decreaseExponential(dt,v.opacity,0.93)
					end
				end
				desiredVol = desiredVol - v.opacity * 0.5
				if v.opacity >= 0.95 then 
					visiblePlayers = visiblePlayers + 1
				else
					semiVisiblePlayers = semiVisiblePlayers + 1
				end
			end


			if semiVisiblePlayers == 0 and visiblePlayers > 1 then
				gameon = true
				started = true
				initialSpawn()
				Sounds.maintheme:stop()
				playSound(Sounds.spawnplayer)
			else
				Sounds.maintheme:setVolume(desiredVol)
			end

		end
	end


	for i,v in ipairs(Players) do
		if gameon then
			v.pos = v.newPos
		end
		if v.dead then
			v.deathtimer = v.deathtimer - dt
			updateAnim(dt, Players.deathsprites)
		else
			updateAnim(dt, v.sprites)
		end
	end

end

function drawPlayers()
	love.graphics.setColor(255,255,255)


	for i,v in ipairs(Players.spawnsprites) do
		if v then
			v.batch:clear()
			v.batch:add(v.frames[v.framendx],v.pos.x,v.pos.y,0,spriteScale.x,spriteScale.y,v.frameside/2,v.frameside/2)
			love.graphics.draw(v.batch)
		end
	end

		if not gameon and not started then
		-- extra info
		love.graphics.draw(ExtraImgs.infop1, 34*6,35*6, 0, spriteScale.x, spriteScale.y)
		love.graphics.draw(ExtraImgs.infop2, 200*6,35*6, 0, spriteScale.x, spriteScale.y)
		love.graphics.draw(ExtraImgs.arrow, 70*6,60*6 + math.sin(ExtraImgs.arrowphase)*20, 0, spriteScale.x, spriteScale.y)
		love.graphics.draw(ExtraImgs.arrow, 230*6,60*6 + math.sin(ExtraImgs.arrowphase)*20, 0, spriteScale.x, spriteScale.y)
	end


	for i,v in ipairs(Players) do
		if v.opacity < 1 then
			love.graphics.setColor(255,255,255,255*v.opacity)
		end
		if v.dead then
			Players.deathsprites.batch:clear()
			Players.deathsprites.batch:add(Players.deathsprites.frames[Players.deathsprites.framendx], v.pos.x,v.pos.y,0,spriteScale.x,spriteScale.y,Players.deathsprites.frameside/2,Players.deathsprites.frameside/2)
			love.graphics.draw(Players.deathsprites.batch)
		else
			v.sprites.batch:clear()
			v.sprites.batch:add(v.sprites.frames[v.sprites.framendx], v.pos.x,v.pos.y,0,spriteScale.x,spriteScale.y,v.sprites.frameside/2,v.sprites.frameside/2)
			love.graphics.draw(v.sprites.batch)
		end
		if v.opacity < 1 then
			love.graphics.setColor(255,255,255,255)
		end

	end


end