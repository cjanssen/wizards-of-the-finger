Enemies = {}
function initEnemies()
	Enemies.list = {}
	Enemies.spawner = {
		followerTimer = 0,
		followerDelay = 0.5,
		wandererTimer = 5,
		wandererDelay = 5
	}
	Enemies.spawnPositions = {
		followers = { {1/8,1/8},{3/8,1/8},{5/8,1/8},{7/8,1/8},{1/8,4/8},{7/8,4/8}, {1/8,7/8},{3/8,7/8},{5/8,7/8},{7/8,7/8}},
		wanderers = { {2/8,1/8},{4/8,1/8},{6/8,1/8},{2/8,7/8},{4/8,7/8},{6/8,7/8},{1/8,5/16},{1/8,11/16},{7/8,5/16},{7/8,11/16}}

		-- followers = { {1/16,1/16},{3/8,1/16},{5/8,1/16},{15/16,1/16},{1/16,4/8},{15/16,4/8}, {1/16,15/16},{3/8,15/16},{5/8,15/16},{15/16,15/16}},
		-- wanderers = { {2/8,1/16},{4/8,1/16},{6/8,1/16},{2/8,15/16},{4/8,15/16},{6/8,15/16},{1/16,5/16},{1/16,11/16},{15/16,5/16},{15/16,11/16}}
	}

	prepareEnemyFrames()
end

function prepareEnemyFrames()
	Enemies.spriteSets = {
		getSprites("img/spriteBlueenemy.png"),
		getSprites("img/spriteRedenemy.png"),
		getSprites("img/spriteGhost.png"),
		getSprites("img/blueenemyDeath.png"),
		getSprites("img/redenemyDeath.png"),
		getSprites("img/ghostdisappear.png")
	}

	Enemies.spawnsprites = getSprites("img/spawn.png")
	Enemies.spawnsprites.loops = false
end

function enemyColor(n)
	if n == 1 then
		return {0,0,255}
	elseif n == 2 then
		return {255,0,0}
	end
end

function startEnemies()
	Enemies.list = {}
	Enemies.followList = {}
	for i,v in ipairs(Players) do Enemies.followList[i] = {} end

	Enemies.spawner.followerTimer = Enemies.spawner.followerDelay
	Enemies.spawnPools = {
		followers = {},
		wanderers = {}
	}
end

function populateSpawnPool(pool, ref)
	if table.getn(pool) == 0 then
		for i,v in ipairs(ref) do
			table.insert(pool,v)
		end
	end
end

function initialSpawn()
    local startingGhosts = 2
    for i=1,startingGhosts do
        newWandererEnemy()
    end
 end


function newEnemy(typ)
	if typ == 1 then
		newFollowerEnemy()
	elseif typ == 2 then
		newWandererEnemy()
	end
end

function newFollowerEnemy(startPos)
	if not startPos then
		populateSpawnPool(Enemies.spawnPools.followers, Enemies.spawnPositions.followers)
		local coord = table.remove(Enemies.spawnPools.followers,math.random(table.getn(Enemies.spawnPools.followers)))
		startPos = {x = screenSize.x * coord[1], y = screenSize.y * coord[2]}
	end

	local _target = math.random(table.getn(Players))
	local enemy = {
		etype = 1,
		state = 0,
		pos =  {x = startPos.x, y = startPos.y},
		newPos =  {x = startPos.x, y = startPos.y},
		target = Players[_target],
		id = _target,
		angle = 0,
		speed = 150,
		rad = 40,
		walksprites = referSprites(Enemies.spriteSets[_target]),
		deathsprites = referSprites(Enemies.spriteSets[_target+3]),
		spawnsprites = referSprites(Enemies.spawnsprites),
		currentSprites = nil
	}

	enemy.timer = Enemies.spawnsprites.framedelay * Enemies.spawnsprites.framecount

	table.insert(Enemies.list, enemy)
	table.insert(Enemies.followList[_target], enemy)
	playSound(Sounds.spawnmonster)
end

function newWandererEnemy(startPos)
	local ang = math.random()*2*math.pi
	if not startPos then
		populateSpawnPool(Enemies.spawnPools.wanderers, Enemies.spawnPositions.wanderers)
		local coord = table.remove(Enemies.spawnPools.wanderers,math.random(table.getn(Enemies.spawnPools.wanderers)))
		startPos = {x = screenSize.x * coord[1], y = screenSize.y * coord[2]}
	end

	local enemy = {
		etype = 2,
		state = 0,
		pos = {x = startPos.x, y = startPos.y},
		newPos =  {x = startPos.x, y = startPos.y},
		dir = {x = math.cos(ang), y = math.sin(ang)},
		angle = 0,
		speed = 150,
		rad = 40,
		walksprites = referSprites(Enemies.spriteSets[3]),
		deathsprites = referSprites(Enemies.spriteSets[3+3]),
		spawnsprites = referSprites(Enemies.spawnsprites),
		currentSprites = nil

	}

	enemy.timer = Enemies.spawnsprites.framedelay * Enemies.spawnsprites.framecount
	table.insert(Enemies.list,enemy)
	playSound(Sounds.spawnmonster)
end

function updateEnemies(dt)
	if gameon then
		updateFull(dt)
	else
		updateAnimsOnly(dt)
	end
end

function updateAnimsOnly(dt)
	for i,v in ipairs(Enemies.list) do
		if v.currentSprites and v.state ~= 2 then
			updateAnim(dt, v.currentSprites)
		end
	end
end

function updateFull(dt)
	updateTimers(dt)

	local immortal = false
	for i,v in ipairs(Enemies.list) do
		if v.state == 0 then
			-- spawning
			v.timer = v.timer - dt
			if v.timer <= v.spawnsprites.framedelay*5 then
				v.currentSprites = v.walksprites
			end
			if v.timer <= 0 then
				setEnemyState(v,1)
			end
			updateAnim(dt, v.spawnsprites)
		elseif v.state == 1 then
			-- moving
			v.currentSprites = v.walksprites


			if v.etype == 1 then
				-- follower
				v.angle = math.atan2(v.target.pos.y - v.pos.y, v.target.pos.x - v.pos.x)
				v.dir = { x = math.cos(v.angle), y = math.sin(v.angle) }

				-- avoid other enemies
				local fac = math.pow(table.getn(Enemies.followList[v.id]),-0.5)

				for j,w in ipairs(Enemies.followList[v.id]) do
					if w ~= v and w.target == v.target then
						local dx = v.pos.x - w.pos.x
						local dy = v.pos.y - w.pos.y
						local dist = math.sqrt(dx*dx+dy*dy)

						if dist>1 then
							local ndir = { x = dx / dist, y = dy / dist }
							local pw = math.pow(dist,-0.2)
							v.dir = {x = v.dir.x + ndir.x * fac*pw, y = v.dir.y + ndir.y *fac*pw}
						end
					end
				end

				v.dir = normalize(v.dir)
				v.newPos.x = v.pos.x + v.speed * dt * v.dir.x
				v.newPos.y = v.pos.y + v.speed * dt * v.dir.y
			elseif v.etype == 2 then
				-- wanderers
				v.newPos.x = v.pos.x + v.speed * v.dir.x * dt
				v.newPos.y = v.pos.y + v.speed * v.dir.y * dt
			end

			-- bounce against borders
			if v.newPos.x <= v.rad then
				v.newPos.x = 2 * v.rad - v.newPos.x
				v.dir.x = -v.dir.x
			end
			if v.newPos.x >= screenSize.x-v.rad then
				v.newPos.x = 2 * (screenSize.x- v.rad) - v.newPos.x
				v.dir.x = - v.dir.x
			end
			if v.newPos.y <= v.rad then
				v.newPos.y = 2*v.rad - v.newPos.y
				v.dir.y = -v.dir.y
			end
			if v.newPos.y >= screenSize.y-v.rad then
				v.newPos.y = 2*(screenSize.y-v.rad) - v.newPos.y
				v.dir.y = - v.dir.y
			end

			
			v.pos = v.newPos

			if not immortal then
				for j,w in ipairs(Players) do
					local dSq = cappedDist(v.pos,w.pos,v.rad,w.rad)
					if dSq <= 0 then
						if w == v.target or v.etype == 2 then
							-- startGame()
							killPlayer(w)
							return
						else
							-- killEnemy(v)
							playSound(Sounds.monsterdie)
							setEnemyState(v,2)
						end
					end
				end
			end

		elseif v.state == 2 then
			-- dying
			v.currentSprites = v.deathsprites
			v.timer = v.timer - dt
			if v.timer <= 0 then
				killEnemy(v)
			end
		end

		-- update anims
		if v.currentSprites then
			updateAnim(dt, v.currentSprites)
		end
	end

	purge(Enemies.list)
	for i,v in ipairs(Enemies.followList) do
		purge(v)
	end
end

function setEnemyState(enemy, state)
	enemy.state = state
	if enemy.state == 2 then
		enemy.timer = 0.5
		enemy.deathsprites.framedelay = enemy.timer/ enemy.deathsprites.framecount
		enemy.deathsprites.framendx = 1
		enemy.deathsprites.frametimer = enemy.deathsprites.framedelay
		enemy.deathsprites.loops = false
	end
end

function updateTimers(dt) 
	local cnt = 1
	for i,v in ipairs(Enemies.followList) do cnt = cnt + table.getn(v) end

	Enemies.spawner.followerTimer = Enemies.spawner.followerTimer - dt * math.pow(cnt,-0.7)
	if Enemies.spawner.followerTimer <= 0 then
		Enemies.spawner.followerTimer = Enemies.spawner.followerTimer + Enemies.spawner.followerDelay
		newEnemy(1)
	end

	Enemies.spawner.wandererTimer = Enemies.spawner.wandererTimer - dt
	if Enemies.spawner.wandererTimer <= 0 then
		Enemies.spawner.wandererTimer = Enemies.spawner.wandererTimer + Enemies.spawner.wandererDelay
		if math.random(4) == 1 then
			-- delete one enemy randomly
			for i,v in ipairs(Enemies.list) do
				if v.etype == 2 then
					-- killEnemy(v)
					playSound(Sounds.ghostdie)
					setEnemyState(v,2)
					break
				end
			end
		else
			-- spawn new enemy
			newEnemy(2)
		end
	end
end

function killEnemy(enemy)
	enemy.dead = true
end

function spawnAnimation(_pos, _anim)

end

function drawEnemies()
	for i,v in ipairs(Enemies.spriteSets) do
		v.batch:clear()
	end

	Enemies.spawnsprites.batch:clear()

	for i,v in ipairs(Enemies.list) do
		if v.state == 0 then
			-- spawning
			v.spawnsprites.batch:add(v.spawnsprites.frames[v.spawnsprites.framendx], v.pos.x, v.pos.y, 0, spriteScale.x, spriteScale.y, v.spawnsprites.frameside/2, v.spawnsprites.frameside/2)
		end
		if v.currentSprites then
			v.currentSprites.batch:add(v.currentSprites.frames[v.currentSprites.framendx], v.pos.x, v.pos.y, 0, spriteScale.x, spriteScale.y, v.currentSprites.frameside/2, v.currentSprites.frameside/2)
		end
	end

	love.graphics.draw(Enemies.spawnsprites.batch)

	for i,v in ipairs(Enemies.spriteSets) do
		love.graphics.draw(v.batch)
	end

end