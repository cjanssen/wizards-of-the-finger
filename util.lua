
function distSq(A,B)
	local dx = A.x - B.x
	local dy = A.y - B.y
	return dx*dx + dy*dy
end

function cappedDist(A,B,da,db)
	return max(0, math.sqrt(distSq(A,B))-(da+db))
end

function min(A,B)
	if A<B then return A else return B end
end

function max(A,B)
	if A>B then return A else return B end
end


function purge(list)
	local n = table.getn(list)
	for i = 0,n-1 do
		if list[n-i].dead then
			table.remove(list,n-i)
		end
	end
end

function normalize(vec)
	local mod = vec.x*vec.x + vec.y*vec.y
	if mod > 0.01 then
		mod = math.sqrt(mod)
		return {x=vec.x/mod,y=vec.y/mod}
	end
	return vec
end

function getSprites(fname_)
	local sp = {}

	local img = love.graphics.newImage(fname_)
	sp.frames = {}
	sp.batch = love.graphics.newSpriteBatch(img,2000,"static")

	local side = img:getWidth()
	local topdown = img:getHeight()
	local fc = math.floor(topdown/side)
	for k=0,fc-1 do
		table.insert(sp.frames,
			love.graphics.newQuad(0, k*side, side, side, side, topdown))
	end

	sp.framendx = 1
	sp.framecount = fc
	sp.frametimer = 0.1
	sp.framedelay = 0.1
	sp.frameside = side
	sp.loops = true
	return sp
end

function referSprites(spriteStruct)
	local sp = {}

	sp.frames = spriteStruct.frames
	sp.batch = spriteStruct.batch
	sp.framendx = 1
	sp.framecount = spriteStruct.framecount
	sp.frametimer = 0.1
	sp.framedelay = 0.1
	sp.frameside = spriteStruct.frameside
	sp.loops = spriteStruct.loops
	return sp
end

function updateAnim(dt, sp)
	sp.frametimer = sp.frametimer - dt
	if sp.frametimer <= 0 then
		sp.frametimer = sp.frametimer + sp.framedelay
		if sp.loops then
			sp.framendx = (sp.framendx % sp.framecount) + 1
		else
			sp.framendx = min(sp.framendx+1, sp.framecount)
		end
	end
end

function increaseExponential(dt, var, amount)
	if var < 1 then
		var = 1 - (1 - var) * math.pow(amount, 60*dt)
		if var > 0.999 then
			var = 1
		end
	end
	return var
end

function decreaseExponential(dt, var, amount)
	if var > 0 then
		var = var * math.pow(amount, 60*dt)
		if var < 0.001 then
			var = 0
		end
	end
	return var
end
