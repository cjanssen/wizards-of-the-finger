Sounds = {}
function initSounds()
	Sounds = {
		deathplayer = loadSound("snd/Deathplayer.ogg"),
		monsterdie = loadSound("snd/monsterdie2.ogg"),
		spawnmonster = loadSound("snd/spawnmonster.ogg"),
		ghostdie = loadSound("snd/ghostdie.ogg"),
		spawnplayer = loadSound("snd/points.ogg"),
		tooclose = love.audio.newSource("snd/proximity2.ogg"),
		maintheme = love.audio.newSource("snd/theme.ogg","stream")
	}
	Sounds.maintheme:setLooping(true)
end

function loadSound(filename)
	local polyphony = {}
	for i=1,5 do
		table.insert(polyphony, love.audio.newSource(filename))
	end
	return polyphony
end

function playSound(set)
	for i=1,5 do
		if set[i]:isStopped() then
			set[i]:rewind()
			set[i]:play()
			return
		end
	end
end

