function loadDependencies()
	love.filesystem.load("util.lua")()
	love.filesystem.load("players.lua")()
	love.filesystem.load("enemies.lua")()
	love.filesystem.load("intro.lua")()
	love.filesystem.load("score.lua")()
	love.filesystem.load("sound.lua")()
end


function love.load()

	loadDependencies()
	-- 320x200 - x3 960x600 - x4 1280x800 - x6 1920×1200
	screenSize = {x = 1920, y = 1200-90}
	OperatingSystem = love.system.getOS()
    if OperatingSystem == "Android" or OperatingSystem == "iOS" then
    	love.window.setFullscreen(true)
    end
	scale = { love.graphics.getWidth() / screenSize.x, love.graphics.getHeight() / screenSize.y }
	love.graphics.setFont(love.graphics.newFont(32))
	love.graphics.setDefaultFilter( "linear", "nearest" )

	font = love.graphics.newImageFont("img/numbers_big.png","0123456789.")
	love.graphics.setFont(font)



	initGame()
	startIntro()

	txt = ""


end

function initGame()
	vibration = 0
	Fingers = {}

	initHighscore()
	initIntro()
	initPlayers()
	initEnemies()
	initBg()
	initSounds()

end

function initBg()
	bgImg = love.graphics.newImage("img/arena.png")
	spriteScale = {x=6,y=6 * (screenSize.y/1200)}
end

function startGame()
	Fingers = {}
	vibration = 0
	startPlayers()
	startEnemies()
end

function love.touchpressed(_b,_x,_y,_p)
	_x = _x / scale[1]
	_y = _y / scale[2]
	if Intro.active then
		introTapped(_x,_y)
	else
		for i,v in ipairs(Players) do
			if distSq({x = _x, y = _y},v.pos) < v.det then
				Fingers[_b] = v
				v.touched = true
			end
		end
	end
end

function love.touchreleased(_b,_x,_y,_p)
	_x = _x / scale[1]
	_y = _y / scale[2]
	if Intro.active then
		introUntapped(_x,_y)
	else
		if Fingers[_b] then
			Fingers[_b].touched = false
		end
		Fingers[_b] = false
	end
end

function love.touchmoved(_b,_x,_y,_p)
	if gameon then
		_x = _x / scale[1]
		_y = _y / scale[2]
		if Fingers[_b] then
			Fingers[_b].newPos = {x = _x, y = _y}
			if _y > screenSize.y then
				Fingers[_b].newPos.y = screenSize.y
			end
		end
	end
end

function love.keypressed(key)
	if key == "escape" then
		if Intro.active then
			if Intro.currentScreen == 2 or Intro.currentScreen == 3 then
				Intro.currentScreen = 1
			else
				love.event.push("quit")
				return
			end
		else
			startIntro()
		end
	end
end

function love.update(dt)
	if dt > 0.1 then
		-- skip when below 10fps
		return
	end


	if Intro.active then
		updateIntro(dt)
	else
		if Intro.fading then
			updateIntro(dt)
		end

		updatePlayers(dt)
		updateEnemies(dt)
	end
end

function love.draw()
	scale = { love.graphics.getWidth() / screenSize.x, love.graphics.getHeight() / screenSize.y }
	love.graphics.push()
	love.graphics.scale(scale[1],scale[2])

	if vibration > 0 then
		love.graphics.push()
		love.graphics.translate((math.random(2)-1.5)*vibration, (math.random(2)-1.5)*vibration)
	end

	if Intro.active then
		drawIntro()
	else

		love.graphics.setColor(255,255,255)
		love.graphics.draw(bgImg,0,0,0,spriteScale.x,spriteScale.y)

		drawPlayers()
		drawEnemies()

		if Intro.fading then
			drawIntro()
		end
	end

	drawScore()

	-- love.graphics.print(love.timer.getFPS(),20,20)

	-- love.graphics.print(table.getn(Enemies.list),20,50)
	-- love.graphics.print(txt,20,80)

	if vibration > 0 then
		love.graphics.pop()
	end

		love.graphics.pop()
end


